package com.fabiodamico.interviews.photocollector.service;


import com.fabiodamico.interviews.photocollector.model.Photo;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by fabdamico.dev on 08/07/2017.
 */
public class PhotoParserServiceTest {

    private Path photosFromFileName;
    private PhotoParserService photoParserService;
    private MapperService mapperService;

    @Before
    public void setUp() throws Exception {
        photosFromFileName = Paths.get(getClass().getClassLoader().getResource("photos").toURI());
        mapperService = new MapperService();
        photoParserService = new PhotoParserService(mapperService);
    }

    @Test
    public void parsePhotosFromString_should_return_14_photoObj() throws IOException, ParseException {
        String photosFromString = new String(Files.readAllBytes(photosFromFileName));
        List<Photo> result = photoParserService.parsePhotosFromString(photosFromString);
        assertThat(result.size(), is(14));
    }

}