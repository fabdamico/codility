package com.fabiodamico.interviews.photocollector.service;

import com.fabiodamico.interviews.photocollector.model.Photo;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.time.LocalDateTime;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by fabdamico.dev on 08/07/2017.
 */
public class MapperServiceTest {
    private MapperService mapperService;

    @Before
    public void setUp() throws Exception {
        mapperService = new MapperService();
    }

    @Test
    public void mapperToPhoto_should_return_ProperPhotoObj() throws ParseException {
        String photoAsString = "Eiffel.jpg, Paris, 2015-07-23 08:03:02";
        Photo photo = mapperService.mapperToPhoto(photoAsString);
        assertThat(photo.getName(), is("Eiffel"));
        assertThat(photo.getExtension(), is("jpg"));
        assertThat(photo.getLocation(), is("Paris"));
        assertThat(photo.getDateTime(), is(LocalDateTime.of(2015, 7, 23, 8, 3, 2)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void mapperToPhoto_should_thrown_an_exception_with_null_arg() throws ParseException {
        mapperService.mapperToPhoto(null);
    }
}