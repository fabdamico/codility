package com.fabiodamico.interviews.photocollector.service;

import com.fabiodamico.interviews.photocollector.model.Photo;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by fabdamico.dev on 08/07/2017.
 */
public class GroupServiceTest {

    private GroupService groupService;
    private List<Photo> photoList;
    private LocalDateTime localDateTimeParisOld = LocalDateTime.of(2015, 12, 31, 23, 59);
    private LocalDateTime localDateTimeParisNew = LocalDateTime.of(2016, 12, 31, 23, 59);
    private LocalDateTime localDateTimeMilanOld = LocalDateTime.of(2015, 12, 31, 23, 59);
    private LocalDateTime localDateTimeMilanNew = LocalDateTime.of(2016, 12, 31, 23, 59);

    @Before
    public void setUp() throws Exception {
        groupService = new GroupService();
        photoList = new ArrayList<>();
        photoList.add(new Photo().name("Louvre").extension("jpg").dateTime(localDateTimeMilanNew).location("Paris"));
        photoList.add(new Photo().name("NotreDame").extension("jpg").dateTime(localDateTimeParisOld).location("Paris"));
        photoList.add(new Photo().name("Duomo").extension("bmp").dateTime(localDateTimeMilanOld).location("Milan"));
        photoList.add(new Photo().name("Castello Sforzesco").extension("bmp").dateTime(localDateTimeParisNew).location("Milan"));
    }

    @Test
    public void groupPhotoByLocationAndSortByDate_should_return_a_map_sorted_and_groupedByLocation() {
        Map<String, List<Photo>> groupPhotoByLocationAndSortByDate = groupService.groupPhotoByLocationAndSortByDate(photoList);
        assertThat(groupPhotoByLocationAndSortByDate.size(), is(2));

        assertThat(groupPhotoByLocationAndSortByDate.get("Milan").size(), is(2));
        assertThat(groupPhotoByLocationAndSortByDate.get("Paris").size(), is(2));

        assertThat(groupPhotoByLocationAndSortByDate.get("Paris").get(0).getDateTime(), is(localDateTimeParisOld));
        assertThat(groupPhotoByLocationAndSortByDate.get("Paris").get(1).getDateTime(), is(localDateTimeParisNew));

        assertThat(groupPhotoByLocationAndSortByDate.get("Milan").get(0).getDateTime(), is(localDateTimeMilanOld));
        assertThat(groupPhotoByLocationAndSortByDate.get("Milan").get(1).getDateTime(), is(localDateTimeMilanNew));

    }

    @Test(expected = IllegalArgumentException.class)
    public void groupPhotoByLocationAndSortByDate_should_thrown_an_exception_with_empty_list() {
        groupService.groupPhotoByLocationAndSortByDate(Collections.EMPTY_LIST);
    }
}