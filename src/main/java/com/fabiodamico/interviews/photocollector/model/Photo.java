package com.fabiodamico.interviews.photocollector.model;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by fabdamico.dev on 08/07/2017.
 */
@Data
public class Photo {
    private String name;
    private String extension;
    private String location;
    private LocalDateTime dateTime;

    public Photo name(String name) {
        this.name = name;
        return this;
    }

    public Photo extension(String extension) {
        this.extension = extension;
        return this;
    }

    public Photo location(String location) {
        this.location = location;
        return this;
    }

    public Photo dateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
        return this;
    }
}