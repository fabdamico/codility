package com.fabiodamico.interviews.photocollector.service;

import com.fabiodamico.interviews.photocollector.model.Photo;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by fabdamico.dev on 08/07/2017.
 */
public class MapperService {

    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public Photo mapperToPhoto(String photoAsString) throws ParseException {

        if (StringUtils.isEmpty(photoAsString)) {
            throw new IllegalArgumentException("Null argument passed to mapper");
        }

        String[] values = photoAsString.split(",");
        String[] splitNameFromExtension = values[0].split("\\.");
        String filename = splitNameFromExtension[0].trim();
        String extension = splitNameFromExtension[1].trim();
        String location = values[1].trim();
        LocalDateTime parsedDate = LocalDateTime.parse(values[2].trim(), dateTimeFormatter);

        return new Photo()
                .name(filename)
                .extension(extension)
                .location(location)
                .dateTime(parsedDate);
    }
}
