package com.fabiodamico.interviews.photocollector.service;


import com.fabiodamico.interviews.photocollector.model.Photo;

import java.util.List;
import java.util.Map;

/**
 * Created by fabdamico.dev on 09/07/2017.
 */
public class RenameService {

    public void addDigitToFilename(Map<String, List<Photo>> input) {

        for (String city : input.keySet()) {
            int index = 0;
            int numberOfDigit = String.valueOf(input.get(city).size()).length();
            for (Photo photo : input.get(city)) {
                index++;
                String leftPad = String.format("%0" + numberOfDigit + "d", index);
                String filename = photo.getLocation() + leftPad + "." + photo.getExtension();
                photo.setName(filename);
            }
        }
    }
}
