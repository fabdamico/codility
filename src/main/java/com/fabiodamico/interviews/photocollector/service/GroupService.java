package com.fabiodamico.interviews.photocollector.service;

import com.fabiodamico.interviews.photocollector.model.Photo;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by fabdamico.dev on 08/07/2017.
 */
public class GroupService {

    public Map<String, List<Photo>> groupPhotoByLocationAndSortByDate(List<Photo> photos) {

        if(CollectionUtils.isEmpty(photos)){
            throw new IllegalArgumentException("Empty photos list passed");
        }

        return photos.
                stream()
                .sorted(Comparator.comparing(Photo::getDateTime))
                .collect(Collectors.groupingBy(photo -> photo.getLocation()));
    }
}
