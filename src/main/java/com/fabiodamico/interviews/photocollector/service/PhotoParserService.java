package com.fabiodamico.interviews.photocollector.service;

import com.fabiodamico.interviews.photocollector.model.Photo;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabdamico.dev on 08/07/2017.
 */
public class PhotoParserService {

    private MapperService mapperService;

    public PhotoParserService(MapperService mapperService) {
        this.mapperService = mapperService;
    }

    public List<Photo> parsePhotosFromString(String photosFromString) throws ParseException {

        List<Photo> parsedPhotos = new ArrayList<>();

        if (StringUtils.isEmpty(photosFromString)) {
            throw new IllegalArgumentException("Null argument passed to the parser");
        }

        String[] strings = photosFromString.split("\\n");
        for (String line : strings) {
            parsedPhotos.add(mapperService.mapperToPhoto(line));
        }

        return parsedPhotos;

    }

}
