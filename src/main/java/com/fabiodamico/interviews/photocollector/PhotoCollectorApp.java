package com.fabiodamico.interviews.photocollector;

import com.fabiodamico.interviews.photocollector.model.Photo;
import com.fabiodamico.interviews.photocollector.service.GroupService;
import com.fabiodamico.interviews.photocollector.service.MapperService;
import com.fabiodamico.interviews.photocollector.service.PhotoParserService;
import com.fabiodamico.interviews.photocollector.service.RenameService;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by fabdamico.dev on 08/07/2017.
 */
public class PhotoCollectorApp {


    public static void main(String[] args) throws ParseException, IOException {

        GroupService groupService = new GroupService();
        MapperService mapperService = new MapperService();
        PhotoParserService photoParserService = new PhotoParserService(mapperService);
        RenameService renameService = new RenameService();

        List<Photo> photos = photoParserService.parsePhotosFromString(
                "photo.jpg, Warsaw, 2013-09-05 14:08:15\n" +
                        "john.png, London, 2015-06-20 15:13:22\n" +
                        "myFriends.png, Warsaw, 2013-09-05 14:07:13\n" +
                        "Eiffel.jpg, Paris, 2015-07-23 08:03:02\n" +
                        "pisatower.jpg, Paris, 2015-07-22 23:59:59\n" +
                        "BOB.jpg, London, 2015-08-05 00:02:03\n" +
                        "notredame.png, Paris, 2015-09-01 12:00:00\n" +
                        "me.jpg, Warsaw, 2013-09-06 15:40:22\n" +
                        "a.png, Warsaw, 2016-02-13 13:33:50\n" +
                        "b.jpg, Warsaw, 2016-01-02 15:12:22\n" +
                        "c.jpg, Warsaw, 2016-01-02 14:34:30\n" +
                        "d.jpg, Warsaw, 2016-01-02 15:15:01\n" +
                        "e.png, Warsaw, 2016-01-02 09:49:09\n" +
                        "f.png, Warsaw, 2016-01-02 10:55:32\n" +
                        "g.jpg, Warsaw, 2016-02-29 22:13:11");

        Map<String, List<Photo>> groupPhotoByLocationAndSortByDate = groupService.groupPhotoByLocationAndSortByDate(photos);
        renameService.addDigitToFilename(groupPhotoByLocationAndSortByDate);

        photos.stream()
                .map(photo -> photo.getName())
                .forEach(System.out::println);
    }


}
